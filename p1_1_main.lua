-- AI - P1

dofile("Class.lua")
dofile("Queue.lua")
dofile("Romania-Graph.lua")
dofile("node.lua")
dofile("search.lua")

romania:Print()

print('BFS-Search Ti')

local node = bdfs(romania, 'Bu', 'Ti', 0, false)
local path = node.name
local cost = 0
while node.parent ~= nil do
  path = node.parent.name .. '-' .. path
  cost = cost + node.weight
  node = node.parent
end
print('Path: '.. path, 'Cost: ' .. cost)

print('DFS-Search Ti')

node = bdfs(romania, 'Bu', 'Ti', 1, false)
path = node.name
cost = 0
while node.parent ~= nil do
  path = node.parent.name .. '-' .. path
  cost = cost + node.weight
  node = node.parent
end
print('Path: '.. path, 'Cost: ' .. cost)

print('UCS-Search Ti')
node = ucs(romania, 'Bu', 'Ti')
local nodeB = node
path = nodeB.name
while nodeB.parent ~= nil do
  path = nodeB.parent.name .. '-' .. path
  nodeB = nodeB.parent
end
print('Path: '.. path, 'Cost: ' .. node.weight)

