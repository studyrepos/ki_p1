-- Node

Node = {}
local Node_mt = Class(Node)


-- Node
function Node:new(name, weight, parent)
  local name = name or nil
  local weight = weight or nil
  local parent = parent or nil
  
  return setmetatable( {name = name, weight = weight, parent = parent}, Node_mt)
end

function Node:equals(arg, value)
  if arg == nil then return false end
  if value then
    return self.name == arg.name and self.weight == arg.weight
  else
    return self.name == arg.name
  end
end

-- comparison result
-- self <= node : true
-- self > node  : false
function Node:compare(node)
  if node == nil then
    return true
  else
    return self.weight <= node.weight
  end
end

function Node:toString()
  return self.name .. ' : ' .. self.weight
end

function Node:print()
  print('Node:', self:toString())
end


