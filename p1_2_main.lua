-- AI - P1

dofile("Class.lua")
dofile("Queue.lua")
dofile("world.lua")

-- build world
local world = World:new('world', 20, 20)

-- start
world:setStart(1,1)
-- goal
world:setGoal(20,20)

-- obstacles
world:setObstacle(10,1)
world:setObstacle(10,2)
world:setObstacle(10,3)
world:setObstacle(10,4)
world:setObstacle(10,5)
world:setObstacle(10,6)
world:setObstacle(10,7)
world:setObstacle(10,8)
world:setObstacle(10,9)
world:setObstacle(10,10)
world:setObstacle(9,10)
world:setObstacle(8,10)
world:setObstacle(7,10)
world:setObstacle(6,10)
world:setObstacle(5,10)

world:setObstacle(17,10)
world:setObstacle(17,11)
world:setObstacle(17,12)
world:setObstacle(17,13)
world:setObstacle(17,14)
world:setObstacle(17,15)
world:setObstacle(17,16)
world:setObstacle(17,17)
world:setObstacle(17,18)
world:setObstacle(17,19)
world:setObstacle(17,20)

-- print world
world:print()
local goal = world:findPath()
world:print()
print(goal:toString())

  


