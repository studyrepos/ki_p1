
dofile "Queue.lua"

-- Field
Field = {}
local Field_mt = Class(Field)

-- type (t):
-- 0 - empty field
-- 1 - obstacle
-- 2 - start
-- 3 - goal
-- 4 - path
function Field:new(posX, posY, t, parent, g, h, p)
  local x = posX    -- position x
  local y = posY    -- position y
  local p = p or 0  -- real + heuristic pathcost
  local g = g or 0  -- real pathcost
  local h = h or 0  -- heuristic pathcost
  local t = t or 0  -- type of field
  local parent = parent or nil
  
  return setmetatable( {
      x = x,
      y = y,
      p = p,
      g = g,
      h = h,
      t = t,
      parent = parent}, Field_mt)
end

function Field:copy()
  return Field:new(self.x, self.y, self.t, self.parent, self.g, self.h, self.p)
end


-- self <= arg || arg == nil  : true
-- self > arg                 : false
function Field:compare(arg)
  if arg == nil then return true
  else
    return self.p <= arg.p
  end
end


function Field:equals(arg, all)
  if arg == nil then return false end
  if all then
    return self.x == arg.x 
            and self.y == arg.y
            and self.t == arg.t
            and self.h == arg.h
            and self.p == arg.p
            and self.g == arg.g
            and self.parent == arg.parent
  else
    return self.x == arg.x and self.y == arg.y
  end
end

function Field:toString()
  return '[' .. self.t .. '-(' .. self.x .. '/' .. self.y .. ')-' .. self.g .. ':' .. self.h .. ']'
end

-- World

World = {}
local World_mt = Class(World)

function World:new(name, width, height)
  local name = name or 'world'
  local width = width or 16
  local height = height or 16
  local start = nil
  local goal = nil
  local fields = {}
  local obstacles = {}

  for y=1, height do
    for x=1, width do
      fields[(y-1)*width + x] = Field:new(x, y, 0)
    end
  end
  
  return setmetatable( {
      name = name,
      width = width, 
      height = height,
      start = start,
      goal = goal,
      fields = fields,
      obstacles = obstacles }, World_mt)
end

function World:setStart(x, y)
  self:setField(x, y, 2)
  self.start = Field:new(x, y, 2)
end

function World:setGoal(x, y)
  self:setField(x, y, 3)
  self.goal = Field:new(x, y, 3)
end

function World:setObstacle(x, y)
  self:setField(x, y, 1)
end

function World:setField(x, y, t)
  self.fields[(y-1) * self.width + x].t = t  
end

function World:getField(x, y)
  if x > 0 and x <= self.width 
    and y > 0 and y <= self.height then
    return self.fields[(y-1) * self.width + x]
  else
    return nil
  end
end

function World:getNeighbours(x, y)
  local neighbours = {}
  if x > 1 and self:getField(x-1, y).t ~= 1 then
    table.insert(neighbours, self:getField(x-1, y):copy())
  end
  if x < self.width and self:getField(x+1, y).t ~= 1 then
    table.insert(neighbours, self:getField(x+1, y):copy())
  end
  if y > 1 and self:getField(x, y-1).t ~= 1 then
    table.insert(neighbours, self:getField(x, y-1):copy())
  end
  if y < self.height and self:getField(x, y+1).t ~= 1 then
    table.insert(neighbours, self:getField(x, y+1):copy())
  end
  return neighbours
end

function World:calcHeuristic()
  for y=1, self.height do
    for x=1, self.width do
      self.fields[(y-1) * self.width + x].h = 
        math.sqrt( (self.goal.x - x)^2 + (self.goal.y - y)^2)
    end
  end
end

function World:print()
  local line, spacer
  spacer = '---|'
  for x=1, self.width do
    spacer = spacer .. '---|'
  end
  
  print(self.name, self.width .. 'x' .. self.height)
  print(spacer)
  for y=self.height, 1, -1 do
    line = ''
    if y < 100 then line = line .. ' ' end
    if y < 10  then line = line .. ' ' end
    line = line .. y .. '|'
    for x=1, self.width do
      local f = self.fields[(y-1) * self.width + x].t
      if f == 1 then
        line = line .. ' # |'
      elseif f == 2 then
        line = line .. ' S |'
      elseif f == 3 then
        line = line .. ' G |'
      elseif f == 4 then
        line = line .. ' + |'
      else
        line = line .. '   |'
      end
    end
    print(line)
    print(spacer)
  end
  line = '   |'
  for x=1, self.width do
    if x < 100 then line = line .. ' ' end
    if x < 10  then line = line .. ' ' end
    line = line .. x .. '|'
  end
  print(line)
end


function World:findPath()
  if self.start == nil or self.goal == nil then
    print("No start or goal!")
    return false
  end
  
  -- recalculate heuristic
  self:calcHeuristic()
  
  -- A* implementation to find path
  -- select start field
  local field = self.start
  -- check for start == goal
  if field:equals(self.goal) then
    print("Found!")
    return field
  end
  
  -- init queues frontier and explored, reset pathCosts
  local pathCost = 0
  local explored = Queue:new(0) -- FiFoQueue
  local frontier = Queue:new(2) -- PrioQueue
  -- push startnode as frist node to frontier
  frontier:push(field)

  while 1 do
    -- frontier empty? No more options, goal not found
    if frontier:isEmpty() then
      print('Failed')
      return nil
    end
    -- pop next node from frontier
    field = frontier:pop()
    -- check node for goal
    if field:equals(self.goal) then
      self:markPath(field)
      print('Found', explored:length())
      return field
    end
    -- mark node as explored
    explored:push(field)
    
    -- process childs of node
    for k, child in ipairs(self:getNeighbours(field.x, field.y)) do
      child.g = field.g + 1
      child.p = child.g + child.h
      child.parent = field
      -- process child only if not already in frontier and explored
      if not frontier:contains(child) and not explored:contains(child) then
        frontier:push(child)
      else
        local highest = frontier:getHighest(child)
        if child:compare(highest) then
          frontier:replace(highest, child)
        end
      end
    end
  end
end

function World:markPath(field)
  local path = field.parent
  while path.parent ~= nil do
    self:setField(path.x, path.y, 4)
    path = path.parent
  end
end

  


