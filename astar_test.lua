-- AI - P1

dofile("Class.lua")
dofile("Queue.lua")
dofile("world.lua")

-- build world
local world = World:new('world', 40, 40)

-- start
world:setStart(1,1)
-- goal
world:setGoal(39,39)

-- obstacles
world:setObstacle(10,5)
world:setObstacle(11,5)
world:setObstacle(12,5)
world:setObstacle(13,5)
world:setObstacle(14,5)
world:setObstacle(15,5)
world:setObstacle(16,5)
world:setObstacle(17,5)
world:setObstacle(18,5)
world:setObstacle(19,5)
world:setObstacle(20,5)
world:setObstacle(21,5)
world:setObstacle(22,5)
world:setObstacle(23,5)
world:setObstacle(24,5)
world:setObstacle(25,5)
world:setObstacle(26,5)
world:setObstacle(27,5)
world:setObstacle(28,5)
world:setObstacle(29,5)
world:setObstacle(30,5)
world:setObstacle(10,5)
world:setObstacle(10,6)
world:setObstacle(10,7)
world:setObstacle(10,8)
world:setObstacle(10,9)
world:setObstacle(10,10)
world:setObstacle(11,11)
world:setObstacle(12,12)
world:setObstacle(13,13)
world:setObstacle(14,14)
world:setObstacle(15,15)
world:setObstacle(16,16)
world:setObstacle(17,17)
world:setObstacle(18,18)
world:setObstacle(19,19)
world:setObstacle(20,20)
world:setObstacle(21,21)
world:setObstacle(22,22)
world:setObstacle(23,23)
world:setObstacle(24,24)
world:setObstacle(25,25)
world:setObstacle(26,26)
world:setObstacle(27,27)
world:setObstacle(28,28)
world:setObstacle(29,29)
world:setObstacle(30,30)


world:setObstacle(1,30)
world:setObstacle(2,30)
world:setObstacle(3,30)
world:setObstacle(4,30)
world:setObstacle(5,30)
world:setObstacle(6,30)
world:setObstacle(7,30)
world:setObstacle(8,30)
world:setObstacle(9,30)
world:setObstacle(10,30)
world:setObstacle(11,30)
world:setObstacle(12,30)
world:setObstacle(13,30)
world:setObstacle(14,30)
world:setObstacle(15,30)
world:setObstacle(16,30)

world:setObstacle(3,1)
world:setObstacle(3,2)
world:setObstacle(4,3)
world:setObstacle(4,4)
world:setObstacle(5,5)

-- print world
world:print()

local goal = world:findPath()
world:print()
print(goal:toString())

  


