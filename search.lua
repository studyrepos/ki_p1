-- search
dofile("Graph.lua")
dofile("node.lua")
dofile("Queue.lua")

function tableContains(table, value)
  for k, v in ipairs(table) do
    if v:equals(value) then return true end
  end
  return false
end


function bdfs(struct, start, goal, mode, debug)
  local node = Node:new(start)
  if node.name == goal then
    return print('Found')
  end
  
  local explored = {}
  local frontier = Queue:new(mode)
  frontier:push(node)

  while 1 do
    if frontier:isEmpty() then
      print('Failed')
      return nil
    end
    node = frontier:pop()
    table.insert(explored, node)
    
    for k,v in pairs(struct:GetNeighbours(node.name)) do
      local child = Node:new(v[1], v[2], node)
      if not tableContains(explored, child) and
        not frontier:contains(child) then
        if child.name == goal then 
          print('Found')
          return child
        end
        frontier:push(child)
      end
    end
  end
end


-- UCS / A*
-- astar
--    not nil - A*
--    else    - UCS
function ucs(struct, start, goal, debug)
  -- create StartNode
  local node = Node:new(start, 0)
  -- check for start == goal
  if node.name == goal then
    return print('Found')
  end
  
  -- init queue frontier, set explored and pathCosts
  local pathCost = 0
  local explored = {}
  local frontier = Queue:new(2)
  -- push startnode as frist node to frontier
  frontier:push(node)

  while 1 do
    -- frontier empty? No more options, goal not found
    if frontier:isEmpty() then
      print('Failed')
      return nil
    end
    -- pop next node from frontier
    node = frontier:pop()
    -- check node for goal
    if node.name == goal then
      print('Found')
      return node
    end
    -- mark node as explored
    table.insert(explored, node)
    
    -- process childs of node
    for k,v in pairs(struct:GetNeighbours(node.name)) do
        local child = Node:new(v[1], node.weight + v[2], node)
      -- process child only if not already in frontier and explored
      if not frontier:contains(child) and not tableContains(explored, child) then
        frontier:push(child)
      else
        local highest = frontier:getHighest(child)
        if child:compare(highest) then
          frontier:replace(highest, child)
        end
      end
    end
  end
end
